export function getOauth1(url, key, secret) {
    const OAuth = require('oauth-1.0a');
    const crypto = require('crypto');
    const oauth = OAuth({
        consumer: {
            key: key,
            secret: secret
        },
        signature_method: 'HMAC-SHA1',
        hash_function(base_string, key) {
            return crypto
                .createHmac('sha1', key)
                .update(base_string)
                .digest('base64')
        }
    });
    const request_data = {
        url: url,
        method: 'GET'
    };
    return {oauth: oauth, request_data: request_data};
}