export function useScrollDirection(entry, previousY = 0, previousRatio = 0) {
    const currentY = entry?.boundingClientRect.y;
    const currentRatio = entry?.intersectionRatio;
    const isIntersecting = entry?.isIntersecting;

    // Scrolling down/up
    if (currentY < previousY) {
        if (currentRatio > previousRatio && isIntersecting) {
            return 'scrollingDownEnter';
        } else {
            return 'scrollingDownLeave';
        }
    } else if (currentY > previousY && isIntersecting) {
        if (currentRatio < previousRatio) {
            return 'scrollingUpLeave';
        } else {
            return 'scrollingUpEnter';
        }
    }
    previousY = currentY;
    previousRatio = currentRatio;
}