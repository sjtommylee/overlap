import { useEffect, useState } from 'react';

export function useWindowSize() {
    const hasWindow = typeof window !== 'undefined';
    function getSize() {
        const width = hasWindow ? window.innerWidth : null;
        const height = hasWindow ? window.innerHeight : null;
        return [width, height];
    }
    const [size, setSize] = useState(getSize());
    useEffect(() => {
        if(hasWindow) {
            function updateSize() {
                setSize([window.innerWidth, window.innerHeight]);
            }
            window.addEventListener('resize', updateSize);
            return () => window.removeEventListener('resize', updateSize);
        }
    }, [hasWindow]);
    return size;
}