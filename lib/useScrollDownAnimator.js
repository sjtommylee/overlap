import {useEffect, useState} from 'react';
import {useInView} from 'react-intersection-observer';
import {useScrollDirection} from './useScrollDirection';

export function useScrollDownAnimator() {
    const [animate, setAnimate] = useState(false);
    const [ref, inView, entry] = useInView({threshold: 1});
    const scrollDirectionEntry = useScrollDirection(entry);
    const scrollDirection = entry ? scrollDirectionEntry : null;

    useEffect(() => {
        if(inView) {
            setAnimate(true);
        }
        else {
            if(scrollDirection != 'scrollingDownLeave') {
                setAnimate(false);
            }
        }
    }, [inView, scrollDirection]);

    return [ref, animate];
}