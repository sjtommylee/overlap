import React, { useContext } from 'react';
import AppContext from 'context/context';
import Head from 'next/head';
import Hero from './hero';
import Footer from './footer';
import Header from './header';
import ReactHtmlParser from 'react-html-parser';
import { useScrollPosition } from 'lib/useScrollPosition';
import styles from './styles/layout.module.css';

const Layout = ({ children, page, nav = [], theme_options }) => {
	const [store, setStore] = useContext(AppContext);
	const currentScrollPosition = useScrollPosition();

	return (
		<>
			<Head>
				{ReactHtmlParser(page.yoast_head)}
				<meta charSet="utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0" />
				<meta name="theme-color" content="#ffffff" />
				<link rel="shortcut icon" type="image/x-icon" href="/imgs/favicon.png" />
				<link rel="shortcut icon" type="image/png" sizes="16x16" href="/imgs/favicon-16x16.png" />
				<link rel="apple-touch-icon" href="/imgs/favicon-57x57.png" />
				<link rel="apple-touch-icon" sizes="114x114" href="/imgs/favicon-114x114.png" />
				<link rel="apple-touch-icon" sizes="72x72" href="/imgs/favicon-72x72.png" />
				<link rel="apple-touch-icon" sizes="144x144" href="/imgs/favicon-144x144.png" />
				<link rel="shortcut icon" href="/imgs/favicon.ico" />
			</Head>
			<div className={`${styles.master} ${currentScrollPosition != 0 ? 'scrolled' : ''}`}>
				<Header pageId={page.id} theme_options={theme_options} nav={nav} />
				{page.acf.hero && page.acf.hero.active ?
					<Hero hero={page.acf.hero} pageId={page.id} />
					: ''}
				<main className={styles.main} role="main">
					{children}
				</main>
				<Footer pageId={page.id} theme_options={theme_options} nav={nav} />
			</div>
		</>
	)
};

export default Layout;