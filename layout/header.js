import { useState } from 'react';
import Link from 'next/link';
import Nav from './nav';

const Header = ({pageId, theme_options, nav}) => {
	const [navOpen, setNavOpen] = useState(false);
	const headerLogo = theme_options.acf.header_logo;

	return (             
		<header id="header" role="banner" className={navOpen ? 'nav_open': ''}>
            <div className="inner">
                <div className="top">
                    {headerLogo ?
                        <div id="logo">
                            <Link href="/"><a>
                                {headerLogo.svg ? <span dangerouslySetInnerHTML={{ __html: headerLogo.svg}} />
                                    : headerLogo.image ? <Image src={headerLogo.image.url} alt={headerLogo.image.alt} width={headerLogo.image.width} height={headerLogo.image.height} loading="eager" /> 
                                    : ''
                                }                            
                            </a></Link>
                        </div>
                    : ''}
                </div>
                <div id="header_right">
                    <Nav id="main_nav" slug="main-menu" menus={nav} />
                </div>
                <span id="nav_icon" onClick={() => setNavOpen(!navOpen)}><span>Nav</span></span>
            </div>
		</header>
	)
};

export default Header;