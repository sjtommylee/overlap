import Link from 'next/link';
import { useRouter } from 'next/router';
import ReactHtmlParser from 'react-html-parser';

const Nav = ({id, slug, menus}) => {
    const router = useRouter();
    const currentSlug = router.asPath;
    const currentCatSlug = router.asPath.split('/')[1];
    let thisMenu = {};

    {menus.map((item, index) => {
        if(item.slug === slug) {
            thisMenu = item.items;
        }
    })};

    if (!thisMenu.length) return <div>ERROR</div>
    return (
        <nav id={id} role="navigation">
            <ul>
                {
                    thisMenu.map((item, index) => 
                        <li key={index} className={`${item.classes.map((x) => `${x}`).join(' ')}${currentSlug+'/' == item.slug ? ' current' : ''}${'/'+currentCatSlug+'/' == item.slug ? ' current_category' : ''}`}>
                            <Link href={item.slug} as={item.slug}><a>{ReactHtmlParser(item.title)}</a></Link>
                            {item.child_items.length ? 
                                <ul>
                                    {
                                        item.child_items.map((child_item, child_index) =>
                                            <li key={index+'_'+child_index} className={`${child_item.classes.map((x) => `${x}`).join(' ')} ${currentSlug+'/' === child_item.slug ? 'current' : ''}`}><Link href={child_item.slug}><a>{ReactHtmlParser(child_item.title)}</a></Link></li>
                                        )
                                    }
                                </ul>
                            : ''}
                        </li>
                    )
                }
            </ul>                        
        </nav>
    )
};

export default Nav;