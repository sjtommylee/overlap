import { useState, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Slider from 'react-slick';
import DynamicWrapper from 'components/dynamic-wrapper';
import ConditionalWrapper from 'components/conditional-wrapper';
import { useWindowSize } from 'lib/useWindowSize';
import { css } from '@emotion/css';
import styles from './styles/hero.module.css';

const Hero = ({ hero, pageId }) => {
	const [windowWidth, windowHeight] = useWindowSize();
	const [screenSize, setScreenSize] = useState('mobile');

	const sliderSettings = {
		fade: true,
		speed: 800,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		pauseOnHover: false
	};

	useEffect(() => {
		setScreenSize(windowWidth > 960 ? 'desktop' : 'mobile');
	}, [windowWidth]);

	const inlineStyles = {
		hero: { backgroundColor: hero.background_color ? hero.background_color : 'inherit' },
	};

	return (
		<section className={`${styles.hero} ${css(inlineStyles.hero)} ${styles[hero.custom_class]} ${styles['page-' + pageId]}`}>
			{hero.images && hero.media_type == 'images' ?
				<Slider {...sliderSettings} className={'hero_slider ' + styles.images}>
					{hero.images.map((imageObj, index) => {
						const image = screenSize == 'desktop' || !imageObj.mobile ? imageObj.desktop : imageObj.mobile;
						const imageStyles = {
							overlay: {
								backgroundColor: imageObj.overlay.color ? imageObj.overlay.color : 'transparent',
								opacity: imageObj.overlay.opacity
							},
						};
						return (
							image.url &&
							<div key={index} className={styles.image}>
								<Image src={image.url} alt={image.alt} layout="fill" objectFit="cover" />
								{imageObj.overlay.color && <div className={`${css(imageStyles.overlay)} ${styles.image_overlay}`}></div>}
							</div>
						)
					})}
				</Slider>
				: ''
			}
			{hero.video && hero.media_type == 'video' ?
				<div className={styles.video}></div>
				: ''
			}
			<div className={styles.content}>
				<DynamicWrapper wrapper={hero.heading_tag}>
					<ConditionalWrapper condition={hero.heading_link} wrapper={children => <Link href={hero.heading_link.url} passHref={hero.heading_link.target === '_blank' ? true : false}><a target={hero.heading_link.target}>{children}</a></Link>}>
						{hero.heading}
					</ConditionalWrapper>
				</DynamicWrapper>
				<DynamicWrapper wrapper={hero.subheading_tag}>
					<ConditionalWrapper condition={hero.heading_link} wrapper={children => <Link href={hero.heading_link.url} passHref={hero.heading_link.target === '_blank' ? true : false}><a target={hero.heading_link.target}>{children}</a></Link>}>
						{hero.subheading}
					</ConditionalWrapper>
				</DynamicWrapper>
				{hero.content || hero.content_secondary ?
					<div className={styles.text}>
						<span dangerouslySetInnerHTML={{ __html: hero.content }} />
						<div className={styles.secondary_content} dangerouslySetInnerHTML={{ __html: hero.content_secondary }} />
					</div>
					: ''
				}
			</div>
		</section>
	)
};

export default Hero;