import Link from 'next/link';
import Nav from './nav';

const Footer = ({pageId, theme_options, nav}) => {
    const currentYear = new Date().getFullYear();
    const footerLogo = theme_options.acf.footer_logo;
    const footerContent = theme_options.acf.footer_content;

    return (             
        <footer id="footer">
            {footerLogo ?
                <div id="footer_logo">                                    
                    <Link href="/"><a>
                        {footerLogo.svg ? <span dangerouslySetInnerHTML={{ __html: footerLogo.svg}} />
                            : footerLogo.image ? <Image src={footerLogo.image.url} alt={footerLogo.image.alt} width={footerLogo.image.width} height={footerLogo.image.height} /> 
                            : ''
                        }                            
                    </a></Link>
                </div>
            : ''}
            <div className="footer_content" dangerouslySetInnerHTML={{ __html: footerContent}}></div>
            <Nav id="footer_nav" slug="footer-menu" menus={nav} />
            <div id="copyright">&copy;{currentYear} &nbsp;|&nbsp; All rights reserved.</div>
            <div className='btn_container'>
                <div>
                    <h5>header</h5>
                    <button className='btn_header'>Start a Project</button>
                </div>
                <div>
                    <h5>Cta</h5>
                    <button style={{marginLeft: '30px', marginRight: '30px'}} className='btn_cta_small'>Learn More</button>
                    <button style={{marginLeft: '30px', marginRight: '30px'}} className='btn_cta1'>Learn More</button>
                    <button style={{marginLeft: '30px', marginRight: '30px'}} className='btn_cta2'>Get Started</button>
                </div>
            </div>
        </footer>
    )
};

export default Footer;