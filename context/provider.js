import React, {useState, useEffect} from 'react';
import AppContext from "./context";

const AppProvider = ({children}) => {
	const [store, setStore] = useState({
        replaceThis: true,
	});

	useEffect(() => {
        const localStore = JSON.parse(localStorage.getItem('store'));
        if(localStore) {
            localStore.replaceThis = false;
        }
	}, []);

    useEffect(() => {
        //Only update for items we want in Local Storage
        localStorage.setItem('store', JSON.stringify(store));
    }, [store.replaceThis]);

	return (
		<AppContext.Provider value={[store, setStore]}>{children}</AppContext.Provider>
	);
};

export default AppProvider;