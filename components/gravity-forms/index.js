import { useState, useEffect } from 'react';
import Textfield from 'components/form-fields/textfield';
import Select from 'components/form-fields/select';
import Textarea from 'components/form-fields/textarea';
import Checkbox from 'components/form-fields/checkbox';
import styles from './gravity-forms.module.css';

const GravityForm = ({form}) => {
    const [formData, setFormData] = useState({});
    let formObj = {};

    //Setup form input data
	useEffect(() => {
        form.fields.map((field, index) => {
            if(field.type === 'checkbox') {
                field.choices.map((choice, choiceIndex) => {
                    const choiceName = 'input_'+field.id+'_'+(choiceIndex+1);
                    if(choice.isSelected) {formObj[choiceName] = choice.value;}
                    else {formObj[choiceName] = '';}
                });
            }
            else {
                formObj['input_'+field.id] = field.defaultValue;
            }
        });
        setFormData(formObj);
    }, []);
    
    const fieldChange = (name, value) => {
        setFormData({...formData, [name]: value});
    };

    const formSubmit = (e) => {
        e.preventDefault();
        fetch(`${process.env.NEXT_PUBLIC_WORDPRESS_URL}/wp-json/gf/v2/forms/${form.id}/submissions`,
            {
                method: 'post',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(formData)
            }
        ).then(res => res.json()
        ).then(console.log(true));
    };
    
    return (
        <form onSubmit={formSubmit}>
            {form.title ? <h3 className={styles.form_title}>{form.title}</h3> : ''}
            {form.description ? <p className={styles.form_description}>{form.description}</p> : ''}
            {form.fields.map((field, index) => {
                const id = 'input_'+form.id+'_'+field.id;
                const name = 'input_'+field.id;
                const label = field.placeholder ? field.placeholder : field.label;
                const type = field.type === 'phone' ? 'tel' : field.type
                return (
                    type === 'address' ? 
                        field.inputs.map((input, index) => {
                            const inputId = input.label.toLowerCase().replace(' ','')+'_'+input.id;
                            const inputLabel = input.placeholder ? input.placeholder : input.label;
                            return (
                                <div className={styles.field+' w_50'} key={input.id}>
                                    <Textfield type={'text'} id={inputId} name={inputId} value={input.defaultValue} label={inputLabel} />
                                </div>
                            );
                        })
                     :
                        <div className={styles.field+' w_50'} key={field.id}>
                            {field.type === 'text' || field.type === 'email' || field.type === 'phone' ? <Textfield type={type} id={id} name={name} value={field.defaultValue} label={label} onChange={fieldChange} /> : ''}
                            {field.type === 'select' ? <Select id={id} name={name} label={label} choices={field.choices} onChange={fieldChange} /> : ''}
                            {field.type === 'textarea' ? <Textarea id={id} name={name} value={field.defaultValue} label={label} onChange={fieldChange} /> : ''}
                            {field.type === 'checkbox' ? <Checkbox formId={form.id} label={label} choices={field.choices} inputs={field.inputs} onChange={fieldChange} /> : ''}
                        </div>
                );
            })}
            <div className={styles.field+' '+styles.submit}>
                <button type="submit">{form.button.text}</button>
            </div>
        </form>
    );
};

export default GravityForm;