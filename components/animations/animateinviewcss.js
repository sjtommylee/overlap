import {useEffect, useState} from 'react';
import {useInView} from 'react-intersection-observer';
import {motion, useAnimation} from 'framer-motion';
import { css } from '@emotion/css';
import {useScrollDirection} from 'lib/useScrollDirection';

const AnimateInViewCSS = ({children, animateType, threshold, element, elementClass, elementStyle, delay}) => {
    //Animation
    const [show, setShow] = useState(false);
    const thresholdVal = threshold ? threshold : 0.2;
    const [ref, inView, entry] = useInView({threshold: thresholdVal});
    const scrollDirectionEntry = useScrollDirection(entry);
    const scrollDirection = entry ? scrollDirectionEntry : null;

    const inlineStyles = {
        animate: {
            opacity: show ? 1 : 0,
            transform: show ? 'translate(0, 0)' : 'translate(0, 50px)'
        }
    };

    useEffect(() => {
        if(inView) {
            setShow(true);
        }
        else {
            if(scrollDirection != 'scrollingDownLeave') {
                setShow(false);
            }
        }
    }, [inView, scrollDirection]);

    return (
        element === 'div' ? 
            <div ref={ref} className={elementClass} style={elementStyle}>
                {children}
            </div> :
        element === 'nav' ? 
            <nav ref={ref} className={elementClass} style={elementStyle}>
                {children}
            </nav> :
        element === 'span' ? 
            <span ref={ref} className={elementClass} style={elementStyle}>
                {children}
            </span> :
        element === 'h1' ? 
            <h1 ref={ref} className={elementClass+' '+css(inlineStyles.animate)} style={elementStyle}>
                {children}
            </h1> :
        element === 'h2' ? 
            <h2 ref={ref} className={elementClass} style={elementStyle}>
                {children}
            </h2> :
        element === 'h3' ? 
            <h3 ref={ref} className={elementClass} style={elementStyle}>
                {children}
            </h3> :
        element === 'h4' ? 
            <h4 ref={ref} className={elementClass} style={elementStyle}>
                {children}
            </h4> :
        element === 'h5' ? 
            <h5 ref={ref} className={elementClass} style={elementStyle}>
                {children}
            </h5>
        : ''
    )
};

export default AnimateInViewCSS;