import { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { motion, useAnimation } from 'framer-motion';
import { useScrollDirection } from 'lib/useScrollDirection';

const AnimateInView = ({ children, animateType, threshold, element, elementClass, elementStyle, delay, staggerChildren }) => {
	//Animation
	const animation = useAnimation();
	const thresholdVal = threshold ? threshold : 0.2;
	const [ref, inView, entry] = useInView({ threshold: thresholdVal });
	const scrollDirectionEntry = useScrollDirection(entry);
	const scrollDirection = entry ? scrollDirectionEntry : null;

	useEffect(() => {
		if (inView) {
			animation.start('visible');
		}
		else {
			if (scrollDirection != 'scrollingDownLeave') {
				animation.start('hidden');
			}
		}
	}, [animation, inView, scrollDirection]);

	const variants = {
		visible: {
			opacity: 1,
			x: 0,
			y: 0,
			transition: {
				duration: 0.5,
				delay: delay,
				ease: "easeOut",
				staggerChildren: !isNaN(staggerChildren) ? staggerChildren : 0
			}
		},
		hidden: {
			opacity: 0,
			x: animateType === 'faderight' ? -50 : animateType === 'fadeleft' ? 50 : 0,
			y: animateType === 'fadeup' ? 20 : 0
		}
	};

	return (
		element === 'div' ?
			<motion.div animate={animation} initial="hidden" variants={variants} ref={ref} className={elementClass} style={elementStyle}>
				{children}
			</motion.div> :
			element === 'nav' ?
				<motion.nav animate={animation} initial="hidden" variants={variants} ref={ref} className={elementClass} style={elementStyle}>
					{children}
				</motion.nav> :
				element === 'span' ?
					<motion.span animate={animation} initial="hidden" variants={variants} ref={ref} className={elementClass} style={elementStyle}>
						{children}
					</motion.span> :
					element === 'h1' ?
						<motion.h1 animate={animation} initial="hidden" variants={variants} ref={ref} className={elementClass} style={elementStyle}>
							{children}
						</motion.h1> :
						element === 'h2' ?
							<motion.h2 animate={animation} initial="hidden" variants={variants} ref={ref} className={elementClass} style={elementStyle}>
								{children}
							</motion.h2> :
							element === 'h3' ?
								<motion.h3 animate={animation} initial="hidden" variants={variants} ref={ref} className={elementClass} style={elementStyle}>
									{children}
								</motion.h3> :
								element === 'h4' ?
									<motion.h4 animate={animation} initial="hidden" variants={variants} ref={ref} className={elementClass} style={elementStyle}>
										{children}
									</motion.h4> :
									element === 'h5' ?
										<motion.h5 animate={animation} initial="hidden" variants={variants} ref={ref} className={elementClass} style={elementStyle}>
											{children}
										</motion.h5>
										: ''
	)
};

export default AnimateInView;