const DynamicWrapper = ({ children, wrapper, wrapperClass, wrapperStyle }) => {
	return (
		wrapper === 'div' ? <div className={wrapperClass} style={wrapperStyle}>{children}</div> :
			wrapper === 'nav' ? <nav className={wrapperClass} style={wrapperStyle}>{children}</nav> :
				wrapper === 'span' ? <span className={wrapperClass} style={wrapperStyle}>{children}</span> :
					wrapper === 'h1' ? <h1 className={wrapperClass} style={wrapperStyle}>{children}</h1> :
						wrapper === 'h2' ? <h2 className={wrapperClass} style={wrapperStyle}>{children}</h2> :
							wrapper === 'h3' ? <h3 className={wrapperClass} style={wrapperStyle}>{children}</h3> :
								wrapper === 'h4' ? <h4 className={wrapperClass} style={wrapperStyle}>{children}</h4> :
									wrapper === 'h5' ? <h5 className={wrapperClass} style={wrapperStyle}>{children}</h5> :
										wrapper === 'h6' ? <h6 className={wrapperClass} style={wrapperStyle}>{children}</h6> :
											wrapper === 'a' ? <a className={wrapperClass} style={wrapperStyle}>{children}</a>
												: ''
	)
};

export default DynamicWrapper;