import React from "react";

const Button = ({
  className,
  onClick,
  type,
  disabled,
  children,
  loading = false,
}) => {
  return <div>{loading ? "loading" : children}</div>;
};

export default Button;
