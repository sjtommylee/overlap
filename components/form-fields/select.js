import React, {useState, useEffect} from 'react';
import styles from './styles/form-fields.module.css';

const Select = ({id, name, label, choices, required, invalidMessage, onChange, attempt}) => {
    const [fieldValue, setFieldValue] = useState('');
    const [dirty, setDirty] = useState('');
    const [invalid, setInvalid] = useState(true);

	const change = (e) => {
		setFieldValue(e.target.value);				
    };
	const focus = (e) => {
		setDirty(true);
	};
	const blur = (e) => {
		if(fieldValue === '') {setDirty(false);}		
	};

	useEffect(() => {
        let valid = true;
        if(required) {
            if(fieldValue === '') {valid = false;}
        }

        if(valid) {setInvalid(false);}
        else {setInvalid(true);}

		if(fieldValue !== '') {
            setDirty(true);            
        }

        onChange({name: name, value: fieldValue, valid: valid});
	}, [fieldValue]);

    //Set default value
	useEffect(() => {
        choices.map((choice, index) => {
            if(choice.isSelected) {setFieldValue(choice.value);}
        })
	}, []);
    
    return (
        <div className={`${styles.fieldwrap} ${styles.select} ${dirty ? styles.dirty : ''} ${invalid && attempt ? styles.invalid : ''}`}>
            <label className={styles.label} htmlFor={id}>
                {label}{invalid && attempt && !dirty ? invalidMessage ? invalidMessage : ' Invalid' : ''}
            </label>
            <select id={id} name={name} value={fieldValue} onChange={change} onFocus={focus} onBlur={blur}>
                {choices.map((choice, index) => 
                    <option key={index} value={choice.value}>{choice.text}</option>
                )}
            </select>
        </div>
    );
};

export default Select;