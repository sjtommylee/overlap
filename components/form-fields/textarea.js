import React, {useState, useEffect} from 'react';
import styles from './styles/form-fields.module.css';

const Textarea = ({id, name, value, label, onChange}) => {
    const [fieldValue, setFieldValue] = useState(value);
    const [dirty, setDirty] = useState('');

	const change = (e) => {
		setFieldValue(e.target.value);				
    };
	const focus = (e) => {
		setDirty(styles.dirty);
	};
	const blur = (e) => {
		if(fieldValue === '') {setDirty('');}		
	};

	useEffect(() => {
        onChange(name, fieldValue);
		if(fieldValue !== '') {setDirty(styles.dirty);}
	}, [fieldValue]);
    
    return (
        <div className={styles.fieldwrap+' '+styles.textarea+' '+dirty}>
            <label className={styles.label} htmlFor={id}>{label}</label>
            <textarea id={id} name={name} value={fieldValue} onChange={change} onFocus={focus} onBlur={blur} />
        </div>
    );
};

export default Textarea;