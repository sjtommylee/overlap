import React, {useState, useEffect} from 'react';
import styles from './styles/form-fields.module.css';

const Checkbox = ({formId, label, choices, inputs, onChange}) => {
    const [checkboxes, setCheckboxes] = useState([]);
    let checkboxesArray = [];

    //Create Checkbox array
	useEffect(() => {
        choices.map((choice, index) => {
            const obj = {...choice, ...inputs[index]};
            if(choice.isSelected) {obj['selected'] = true;}
            else {obj['selected'] = false;}
            checkboxesArray.push(obj);
        });
        setCheckboxes(checkboxesArray);
    }, []);
    
    const change = (e, index) => {
        const newValue = e.target.checked ? e.target.value : '';
        let newCheckboxes = [...checkboxes];
        newCheckboxes[index].selected = !newCheckboxes[index].selected;
        setCheckboxes(newCheckboxes);
        onChange(e.target.name, newValue);
    };
    
    return (
        <div className={styles.fieldwrap+' '+styles.checkboxes}>
            <label className={styles.label}>{label}</label>
            {checkboxes.map((checkbox, index) => {
                const id = checkbox.id.replace('.','_');
                return (
                    <div key={checkbox.id} className={styles.checkbox_field}>
                        <input type="checkbox" id={'choice_'+formId+'_'+id} name={'input_'+id} value={checkbox.value} checked={checkbox.selected} onChange={(e) => change(e, index)} />
                        <label className={styles.checkbox_label} htmlFor={'choice_'+formId+'_'+id}>{checkbox.label}</label>
                    </div>
                )
            })}
        </div>
    );
};

export default Checkbox;