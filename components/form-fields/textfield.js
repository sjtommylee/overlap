import React, {useState, useEffect} from 'react';
import styles from './styles/form-fields.module.css';

const Textfield = ({type, id, name, value, label, required, validation, maxlength, invalidMessage, onChange, attempt}) => {
    const [fieldValue, setFieldValue] = useState(value);
    const [dirty, setDirty] = useState(false);
    const [invalid, setInvalid] = useState(true);

	const change = (e) => {
        if(maxlength) {
            if(maxlength >= e.target.value.length) {
		        setFieldValue(e.target.value);
            }
        }
        else {setFieldValue(e.target.value);}
    };
	const focus = (e) => {
		setDirty(true);
	};
	const blur = (e) => {
		if(fieldValue === '') {
            setDirty(false);
            //if(required) {setInvalid(true);}
        }		
	};

	useEffect(() => {
        let valid = true;
        if(required) {
            if(validation === 'email') {
                if(!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(fieldValue)) {
                    valid = false;
                }
            }

            if(fieldValue === '') {valid = false;}
        }

        if(valid) {setInvalid(false);}
        else {setInvalid(true);}

		if(fieldValue !== '') {
            setDirty(true);            
        }
        
        onChange({name: name, value: fieldValue, valid: valid});
	}, [fieldValue]);
    
    return (
        <div className={`${styles.fieldwrap} ${styles.text} ${dirty ? styles.dirty : ''} ${invalid && attempt ? styles.invalid : ''}`}>
            <label className={styles.label} htmlFor={id}>
                {label}{invalid && attempt && !dirty ? invalidMessage ? invalidMessage : ' Invalid' : ''}
            </label>
            <input type={type} id={id} name={name} value={fieldValue} onChange={change} onFocus={focus} onBlur={blur} />
        </div>
    );
};

export default Textfield;