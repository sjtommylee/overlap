module.exports = {
  reactStrictMode: true,
  images: {
    deviceSizes: [375, 480, 640, 960, 1200, 1600, 1920],
    domains: ["overlap-admin.overlaplab.com", "overlap-vercel.vercel.app"],
  },
};
