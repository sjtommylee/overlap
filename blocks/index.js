import React from 'react';
import RegularContent from './acf/regular-content';
import SlickImageCarousel from './acf/slick-image-carousel';
import AlternatingRows from './acf/alternating-rows';
import ContentGrid from './acf/content-grid';

const Blocks = ({data}) => {
    return (
        <>
            {data.map((block, index) =>
                <React.Fragment key={index}>
                    {block.blockName === 'acf/regular-content' ? <RegularContent attrs={block.attrs} /> : ''}
                    {block.blockName === 'acf/slick-image-carousel' ? <SlickImageCarousel attrs={block.attrs} /> : ''}
                    {block.blockName === 'acf/alternating-rows' ? <AlternatingRows attrs={block.attrs} /> : ''}
                    {block.blockName === 'acf/content-grid' ? <ContentGrid attrs={block.attrs} /> : ''}
                </React.Fragment>
            )}
        </>
    )
};

export default Blocks;