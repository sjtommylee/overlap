import Link from 'next/link';
import Image from 'next/image';
import AnimateInView from 'components/animations/animateinview';
import Slider from 'react-slick';
import { css } from '@emotion/css';
import styles from './styles/content-grid.module.css';
import arrow from 'public/icons/arrow-right.svg'
const ContentGrid = ({attrs}) => {
    const data = attrs.data;
    // console.log('grid_'+index+'_cta' + 'title')
    const inlineStyles = {
        item_outer: {
            '@media (min-width: 961px)': {
                width: 100/data.columns_per_row+'%',
                ':nth-child(2n+1)': { clear: data.columns_per_row == 2 ? 'both' : ''},
                ':nth-child(3n+1)': { clear: data.columns_per_row == 3 ? 'both' : ''},
                ':nth-child(4n+1)': { clear: data.columns_per_row == 4 ? 'both' : ''}
            },
            '@media (max-width: 960px)': {
            }
        },
    };

    const sliderSettings = {
        dots: true,
        infinite: false,
        arrows: false,
        speed: 800,
        slidesToShow: 1,
        slidesToScroll: 1,
        className: styles.grid,
        responsive: [
            {
                breakpoint: 960,
                settings: data.mobile_slider == 1 ? {slidesToShow: 1, slidesToScroll: 1} : 'unslick'
            },
            {
                breakpoint: 1000000,
                settings: 'unslick'
            }
        ]
    };

    return (
        <section className={'block '+styles.section + ' ' + styles[data.custom_class]} id={data.sec_anchor_name}>
            <div className={styles.inner}>
                <div className={styles.top}>
                    {data.heading ?
                        <AnimateInView animateType="fadeup" element={data.heading_tag} elementClass={styles.heading} threshold="1">{data.heading}</AnimateInView>
                    : ''}
                    {data.subheading ?
                        <AnimateInView animateType="fadeup" element={data.subheading_tag} elementClass={styles.subheading} threshold="1">{data.subheading}</AnimateInView>
                    : ''}
                    {data.top_content_content ?
                        <AnimateInView animateType="fadeup" element="div" elementClass={styles.topcontent} threshold="0.2">{data.top_content_content}</AnimateInView>
                    : ''}
                    {data.top_content_cta ?
                        <AnimateInView animateType="fadeup" element="div" elementClass={styles.topcontent_cta} threshold="1" delay="0.25">
                            {data.top_content_cta.target === '_blank' ?
                                <a href={data.top_content_cta.url} target="_blank" className="btn">{data.top_content_cta.title}</a>
                                : <Link href={data.top_content_cta.url}><a className="btn">{data.top_content_cta.title}</a></Link>
                            }
                        </AnimateInView>
                    : ''}
                </div>
                <Slider {...sliderSettings}>
                    {[...Array(data.grid)].map((item, index) => {
                        const imageGroup = 'grid_'+index+'_image_';

                        return (
                            <AnimateInView key={index} animateType="fadeup" element="div" elementClass={styles.item_outer+' '+css(inlineStyles.item_outer)} threshold="0.1" delay={index * 0.5}>
                                <div className={styles.item}>
                                    <div className={styles.image}>
                                        {data[imageGroup+'type'] === 'svg' ? <span dangerouslySetInnerHTML={{ __html: data[imageGroup+'svg']}} />
                                        : data[imageGroup+'type'] === 'file' && data[imageGroup+'file'].url ? <Image src={data[imageGroup+'file'].url} alt={data[imageGroup+'file'].alt} width={data[imageGroup+'file'].width} height={data[imageGroup+'file'].height} /> 
                                        : ''}
                                    </div>
                                    {/* subheading start */}
                                    <span className='info-tag font-small mt_sm'>{data[`grid_` + index +`_subheading`]}</span>
                                    {/* subeading end */}
                                    {data['grid_'+index+'_heading'] ?
                                        <AnimateInView animateType="fadeup" element={data['grid_'+index+'_heading_tag']} elementClass={styles.item_heading} threshold="1">{data['grid_'+index+'_heading']}</AnimateInView>
                                    : ''}
                                    {data['grid_'+index+'_content'] ?
                                        <AnimateInView animateType="fadeup" element="div" elementClass={styles.item_content} threshold="0.2">{data['grid_'+index+'_content']}</AnimateInView>
                                    : ''}        
                                    {/* cta button start */}
                                    <div>
                                        {/* <button className={ data[`grid_` + index + `_CTA_Button`] }>{data[`grid_` + index + `_CTA_Text`]}<span><Image className='arrow-left' alt="->" src={arrow}/></span></button> */}
                                        {data['grid_'+ index + '_cta'].title ? 
                                            <div className={styles.cta_container}>
                                                <div className={styles.cta_btn}>
                                                  <AnimateInView animateType="fadeup" element="div" elementClass={`${styles.cta}`} threshold="0.2">{data['grid_'+index+'_cta'].title}</AnimateInView>
                                                </div>
                                                <div className={styles.cta_arrow}>
                                                  <span className={styles.arrow_span} ><Image alt="->" src={arrow}/></span>
                                                </div>
                                            </div>
                                         : null}

                                    </div>
                                    {/* cta button end */}
                                </div>
                            </AnimateInView>
                        )
                    })}
                </Slider>
                <div className={styles.bottom}>
                    {data.bottom_content_content ?
                        <AnimateInView animateType="fadeup" element="div" elementClass={styles.bottomcontent} threshold="0.2">{data.bottom_content_content}</AnimateInView>
                    : ''}
                    {data.bottom_content_cta ?
                        <AnimateInView animateType="fadeup" element="div" elementClass={styles.bottomcontent_cta} threshold="1" delay="0.25">
                            {data.bottom_content_cta.target === '_blank' ?
                                <a href={data.bottom_content_cta.url} target="_blank" className="btn">{data.bottom_content_cta.title}</a>
                                : <Link href={data.bottom_content_cta.url}><a className="btn">{data.bottom_content_cta.title}</a></Link>
                            }
                        </AnimateInView>
                    : ''}
                </div>
            </div>
        </section>
    )

};

export default ContentGrid;