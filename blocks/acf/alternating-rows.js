import Link from 'next/link';
import Image from 'next/image';
import {useWindowSize} from 'lib/useWindowSize';
import AnimateInView from 'components/animations/animateinview';
import styles from './styles/alternating-rows.module.css';

const AlternatingRows = ({attrs}) => {
    const [windowWidth, windowHeight] = useWindowSize();
    const data = attrs.data;
    const flipClass = data.flip_columns == 1 ? styles.flip : '';

    return (
        <section className={'block '+styles.section + ' ' + styles[data.column_widths] + ' ' + flipClass + ' ' + styles[data.custom_class]} id={data.sec_anchor_name}>
            <div className={styles.inner}>
                {[...Array(data.rows)].map((row, index) => {
                    const flipIndex = data.flip_columns == 1 ? index+1 : index;
                    const mediaAnimateType = flipIndex % 2 == 0 ? 'faderight' : 'fadeleft';
                    const contentGroup = 'rows_'+index+'_content_';
                    const mediaGroup = 'rows_'+index+'_media_';

                    const imageObj = data[mediaGroup+'image_desktop_obj'];
                    const mobileImageObj = data[mediaGroup+'image_mobile_obj'].src ? data[mediaGroup+'image_mobile_obj'] : imageObj;
                    //Load image based on window width
                    const loadedImage = windowWidth > 960 ? imageObj : mobileImageObj;

                    //Inline Styles per item
                    const inlineStyles = {
                        overlay: {
                            backgroundColor: data[mediaGroup+'overlay_color'] ? data[mediaGroup+'overlay_color'] : 'inherit',
                            opacity: data[mediaGroup+'overlay_opacity'] ? data[mediaGroup+'overlay_opacity'] : 0
                        }
                    };
                    return (
                        <div key={index} className={styles.item}>
                            <div className={styles.content}>
                                {data[contentGroup+'heading'] ?
                                    <AnimateInView animateType="fadeup" element={data[contentGroup+'heading_tag']} elementClass={styles.heading} threshold="1">{data[contentGroup+'heading']}</AnimateInView>
                                    : ''}
                                {data[contentGroup+'subheading'] ?
                                    <AnimateInView animateType="fadeup" element={data[contentGroup+'subheading_tag']} elementClass={styles.subheading} threshold="1">{data[contentGroup+'subheading']}</AnimateInView>
                                    : ''}
                                <AnimateInView animateType="fadeup" element="div" elementClass={styles.bodycontent} threshold="0" delay="0.25"><span dangerouslySetInnerHTML={{ __html: data[contentGroup+'content'] }} /></AnimateInView>                                
                                {data[contentGroup+'cta'] ?
                                    <AnimateInView animateType="fadeup" element="div" threshold="1" delay="0.25">
                                        {data[contentGroup+'cta'].target === '_blank' ?
                                            <a href={data[contentGroup+'cta'].url} target="_blank" className="btn">{data[contentGroup+'cta'].title}</a>
                                            : <Link href={data[contentGroup+'cta'].url}><a className="btn">{data[contentGroup+'cta'].title}</a></Link>
                                        }
                                    </AnimateInView>
                                : ''}
                            </div>
                            <div className={styles.media}>
                                {(imageObj || mobileImageObj ?
                                    <AnimateInView animateType={mediaAnimateType} element="div" elementClass={styles.image} threshold="0">
                                        <Image src={loadedImage.src} width={loadedImage.width} height={loadedImage.height} alt={loadedImage.alt} />
                                        <div className={styles.overlay} style={inlineStyles.overlay}></div>
                                    </AnimateInView>
                                    : '')}
                            </div>
                        </div>
                    )
                })}
            </div>
        </section>
    )
};

export default AlternatingRows;