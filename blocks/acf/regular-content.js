import Link from 'next/link';
import styles from './styles/regular-content.module.css';
const chalk = require('chalk')
/**
 * 
 * Saw that some regular-content sections had a small divider, where as some do not. 
 * added a div with the classname "divider", which can be used with adding a custom class attribute of "divider" in the WP admin dashboard
 *  
 */

const RegularContent = ({attrs}) => {

    const data = attrs.data;
    const inlineStyles = {
        backgroundColor: data.background_color ? data.background_color : 'inherit',
        color: data.text_color ? data.text_color : 'inherit'
    };

    return (
        <section  className={'block '+styles.regular_content + ' ' + data.custom_class} id={data.sec_anchor_name} style={inlineStyles}>
            <div className={styles.inner}>
                <div className="fade_up no_offset">
                    {data.headline ? <h2>{data.headline}</h2> : ''}
                    {data.custom_class === 'divider' ? (<div className={styles.divider}/>) : null  }
                    {data.content ? <div className={styles.content} dangerouslySetInnerHTML={{ __html: data.content }} /> : ''}
                    {data.cta_button.title && data.cta_button.url ?
                        <p>
                            {data.cta_button.target === '_blank' ?
                                <a href={data.cta_button.url} target="_blank" className="btn">{data.cta_button.title}</a>
                                : <Link href={data.cta_button.url}><a className="btn">{data.cta_button.title}</a></Link>
                            }
                        </p>
                    : ''}
                </div>
            </div>
        </section>
    )
};

export default RegularContent;
