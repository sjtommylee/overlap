import Slider from 'react-slick';
import Image from 'next/image';
import styles from './styles/slick-image-carousel.module.css';

const SlickImageCarousel = ({attrs}) => {
    const data = attrs.data;

    const sliderSettings = {
        dots: true,
        infinite: true,
        arrows: false,
        speed: 800,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 640,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }]
    };

    return (
        <section className={'block '+styles.slick_image_carousel + ' ' + data.custom_class} id={data.sec_anchor_name}>
            <Slider {...sliderSettings} className={'rotator '+styles.images}>
                {[...Array(data.images)].map((image, index) => {
                    const imageObj = data['images_'+index+'_image_obj'];
                    if(imageObj) {
                        return (
                            <div key={index} className={styles.image}>
                                <Image src={imageObj.src} width={imageObj.width} height={imageObj.height} alt={imageObj.alt} />
                            </div>
                        )
                    }
                })}
            </Slider>
        </section>
    )
};

export default SlickImageCarousel;