import Layout from 'layout';
import Blocks from 'blocks';
import styles from 'styles/pages/home.module.css';

const Home = ({page, blocks, nav, theme_options}) => {
    return (
        <Layout nav={nav} page={page} theme_options={theme_options}>
            {page.content.rendered ? 
                <section className={styles.home} id="content">
                    <div dangerouslySetInnerHTML={{ __html: page.content.rendered}} />
                </section>
            : ''}
            {blocks && blocks.length ? 
                <Blocks data={blocks} />
            : ''}
        </Layout>
    )
};

export default Home;

export async function getStaticProps() {
    const [page, blocks, nav, theme_options] = await Promise.all([
        fetch(`${process.env.WORDPRESS_URL}/wp-json/wp/v2/pages/2`).then(res => res.json()),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/overlap/v2/blocks/2`).then(res => res.json()),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/overlap/v2/menus/`).then(res => res.json()),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/acf/v3/options/options`).then(res => res.json()),
    ]);
    return {
        props: { page, blocks, nav, theme_options },
        revalidate: 1
    };
};