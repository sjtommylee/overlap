import Layout from 'layout';
import Blocks from 'blocks';
import Form from 'components/gravity-forms';
import {getOauth1} from 'lib/oauth1';

const Contact = ({page, blocks, form, nav, theme_options}) => {
    return (
        <Layout nav={nav} page={page} theme_options={theme_options}>
            <section id="content">
                <h1>{page.title.rendered}</h1>
                <div dangerouslySetInnerHTML={{ __html: page.content.rendered }} />
                {form.fields ?
                    <Form form={form} />
                : ''}
            </section>
            {blocks && blocks.length ?
                <Blocks data={blocks} />
            : ''}
        </Layout>
    )
};

export default Contact;

export async function getStaticProps() {
    //Needed to render any forms (import getOauth1 above)
    const formURL = `${process.env.WORDPRESS_URL}/wp-json/gf/v2/forms/1`;
    const oauth1 = getOauth1(formURL, process.env.GF_KEY, process.env.GF_SECRET);

    const [page, blocks, form, nav, theme_options] = await Promise.all([
        fetch(`${process.env.WORDPRESS_URL}/wp-json/wp/v2/pages/?slug=contact`).then(res => res.json()).then(result => result[0]),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/overlap/v2/blocks/?slug=contact`).then(res => res.json()),
        fetch(formURL, {headers: {...oauth1.oauth.toHeader(oauth1.oauth.authorize(oauth1.request_data))}}).then(res => res.json()),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/overlap/v2/menus/`).then(res => res.json()),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/acf/v3/options/options`).then(res => res.json()),
    ]);
    return {
        props: {
            page, blocks, form, nav, theme_options
        },
        revalidate: 1
    };
};