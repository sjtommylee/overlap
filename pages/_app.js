import AppProvider from 'context/provider';
import {AnimateSharedLayout} from 'framer-motion';
import 'styles/base.css';
import 'styles/slick.css';

const MyApp = ({ Component, pageProps }) => {
    return (
        <AppProvider>
            <AnimateSharedLayout type="crossfade">
                <Component {...pageProps} />
            </AnimateSharedLayout>
        </AppProvider>
    ) 
}

export default MyApp;