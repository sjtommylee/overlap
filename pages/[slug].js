import Layout from 'layout';
import Blocks from 'blocks';

const Page = ({page, blocks, nav, theme_options}) => {
    return (
        <Layout nav={nav} page={page} theme_options={theme_options}>
            {page.content.rendered ? 
                <section id="content">
                    <h1>{page.title.rendered}</h1>
                    <div dangerouslySetInnerHTML={{ __html: page.content.rendered }} />          
                </section>
            : ''}
            {blocks && blocks.length ?
                <Blocks data={blocks} />
            : ''}
        </Layout>
    );
};

export default Page;

export async function getStaticPaths() {
    const res = await fetch(process.env.WORDPRESS_URL+'/wp-json/wp/v2/pages');
    const pages = await res.json();

    //Add slugs of custom pages at root level to this array
    const customPages = ['contact', '404'];

    //Remove custom pages from array to prevent build errors
    const cleanPages = [];
    pages.map((page, index) => {
        if(!customPages.includes(page.slug)) {cleanPages.push(page);}
    });

    const paths = cleanPages.map((page) => ({
        params: { slug:page.slug }
    }));
    return { paths, fallback:false };
};

export async function getStaticProps({ params }) {
    const [page, blocks, nav, theme_options] = await Promise.all([
        fetch(`${process.env.WORDPRESS_URL}/wp-json/wp/v2/pages?slug=${params.slug}`).then(res => res.json()).then(result => result[0]),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/overlap/v2/blocks/?slug=${params.slug}`).then(res => res.json()),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/overlap/v2/menus`).then(res => res.json()),
        fetch(`${process.env.WORDPRESS_URL}/wp-json/acf/v3/options/options`).then(res => res.json()),
    ]);

    return {
        props: { page, blocks, nav, theme_options },
        revalidate: 1
    };
};